package mx.unitec.moviles.practica3

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity as AppCompatActivity1

class SpinnerActivity : AppCompatActivity1(), AdapterView.OnItemSelectedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spinner)

        val spinner: Spinner = findViewById(R.id.spinnerAlcaldia)

        spinner.onItemSelectedListener = this

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.alcaldia_array,
            android.R.layout.simple_spinner_item
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner.adapter = adapter

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        Toast.makeText( this,
            position.toString() + ": " + parent?.getItemAtPosition(position).toString(),
            Toast.LENGTH_SHORT).show()
    }
}